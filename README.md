# S3 with Minio

## Install

```bash
kubectl create ns minio
helm upgrade  minio stable/minio -f minio-values.yaml  --namespace minio --install
```
